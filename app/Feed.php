<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'url'
    ];

    /**
     * Remove trailed slash in url.
     *
     * @param string $url
     * @return string
     */
    public static function removeTrailedSlash($url)
    {
        return rtrim($url, '\/');
    }

    /**
     * Load feeds from provided url.
     *
     * @param string $url
     * @return array
     */
    public static function loadFeedsFromUrl($url)
    {
        try {
            $xmlData = file_get_contents($url);
            $articles = [];

            if ($xmlData !== false) {
                $xmlData = json_decode(json_encode(simplexml_load_string($xmlData)));

                if (is_object($xmlData)) {
                    if (isset($xmlData->channel) && isset($xmlData->channel->item)) {
                        foreach ($xmlData->channel->item as $item) {
                            $articles[] = [
                                'title' => !empty($item->title) ? $item->title : '',
                                'pubDate' => !empty($item->pubDate) ? $item->pubDate : '',
                                'link' => !empty($item->link) ? $item->link : ''
                            ];
                        }
                    }

                    if (isset($xmlData->entry)) {
                        foreach ($xmlData->entry as $entry) {
                            $articles[] = [
                                'title' => !empty($entry->title) ? $entry->title : '',
                                'pubDate' => !empty($entry->published) ? $entry->published : '',
                                'link' => !empty($entry->link) && !empty($entry->link) && !empty($entry->link->{'@attributes'}) && !empty($entry->link->{'@attributes'}->href) ? $entry->link->{'@attributes'}->href : ''
                            ];
                        }
                    }
                }
            }

            return $articles;
        } catch (\Exception $e) {
            return [];
        }
    }
}
