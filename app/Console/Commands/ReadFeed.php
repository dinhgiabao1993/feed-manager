<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use Validator;

class ReadFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:read {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read feed by ID.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $headers = [
                'Title',
                'Published Date'
            ];
            $id = $this->argument('id');

            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer|min:1',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();

                foreach ($errors->get('id') as $message) {
                    $this->error($message);
                }

                return;
            }

            $feed = Feed::where('id', $id)->first();

            if (!$feed) {
                return $this->info('No feeds found.');
            }

            $articles = Feed::loadFeedsFromUrl($feed->url);

            if (!count($articles)) {
                return $this->info('No articles found.');
            }

            foreach ($articles as &$article) {
                unset($article['link']);
            }

            $this->table($headers, $articles);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
