<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use Validator;

class AddFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new feed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $title = $this->askTitle();
            $url = $this->askUrl();

            $feed = new Feed;
            $feed->title = $title;
            $feed->url = Feed::removeTrailedSlash($url);
            $saved = $feed->save();

            if ($saved) {
                $this->info('Feed successfully added.');

                $headers = [
                    'ID',
                    'Title',
                    'URL'
                ];

                return $this->table($headers, [
                    [
                        'id' => $feed->id,
                        'title' => $feed->title,
                        'url' => $feed->url
                    ]
                ]);
            } else {
                throw new Exception('Feed unsuccessfully added.');
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Ask and validate Feed title.
     *
     * @return string
     */
    private function askTitle()
    {
        $title = $this->ask('What is your feed title?');
        $titleValidator = Validator::make(['title' => $title], ['title' => 'required|max:191']);

        if ($titleValidator->fails()) {
            $errors = $titleValidator->errors();

            foreach ($errors->get('title') as $message) {
                $this->error($message);
            }

            return $this->askTitle();
        }

        return $title;
    }

    /**
     * Ask and validate Feed url.
     *
     * @return string
     */
    private function askUrl()
    {
        $url = $this->ask('What is your feed URL?');
        $urlValidator = Validator::make(['url' => $url], ['url' => 'required|url|max:191|unique:feeds']);

        if ($urlValidator->fails()) {
            $errors = $urlValidator->errors();

            foreach ($errors->get('url') as $message) {
                $this->error($message);
            }

            return $this->askUrl();
        }

        return $url;
    }
}
