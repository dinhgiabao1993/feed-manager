<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use Validator;

class ReadFeedUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:read-url {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read Feed by url.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $headers = [
                'Title',
                'Published Date'
            ];
            $url = Feed::removeTrailedSlash($this->argument('url'));

            $validator = Validator::make(['url' => $url], [
                'url' => 'required|url|max:191',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();

                foreach ($errors->get('url') as $message) {
                    $this->error($message);
                }

                return;
            }

            $feed = Feed::where('url', $url)->first();

            if (!$feed) {
                if (!$this->confirm('No feeds found. Do you still want to read this URL?')) {
                    return;
                }
            }

            $articles = Feed::loadFeedsFromUrl($url);

            if (!count($articles)) {
                return $this->info('No articles found.');
            }

            foreach ($articles as &$article) {
                unset($article['link']);
            }

            $this->table($headers, $articles);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
