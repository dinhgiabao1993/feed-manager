<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use Validator;

class DeleteFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:delete {ids*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $ids = $this->argument('ids');

            $validator = Validator::make(['ids' => $ids], [
                'ids' => 'required|array|min:1',
                'ids.*' => 'required|integer|min:1|distinct',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();

                foreach ($errors->get('ids.*') as $ids) {
                    foreach ($ids as $error) {
                        $this->error($error);
                    }
                }

                return;
            }

            if (!$this->confirm('Are you sure about deleting these feeds (IDs: ' . implode(', ', $ids) . ')')) {
                return $this->info('Canceled.');
            } else {
                $deleted = Feed::destroy($ids);

                if ($deleted) {
                    $this->info('Feeds successfully deleted.');
                } else {
                    throw new Exception('Feeds successfully deleted.');
                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
