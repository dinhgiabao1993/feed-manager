<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;

class ListFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $headers = [
                'ID',
                'Title',
                'URL'
            ];
            $feeds = Feed::get(['id', 'title', 'url']);

            if (!count($feeds)) {
                $this->info('No feeds found.');
            }

            $this->table($headers, $feeds);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
