<?php

namespace App\Http\Controllers;

use App\Feed;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = \DB::table('feeds');

        if ($sign = $request->input('query')) {
            $query = $query->where('url', 'like', '%' . $sign . '%')
                ->orWhere('title', 'like', '%' . $sign . '%');
        }

        $feeds = $query->get();

        return view('feed.index', [
            'title' => 'Feeds',
            'feeds' => $feeds
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $feed = new Feed;

        return view('feed.create', [
            'title' => 'New Feed',
            'feed' => $feed
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:191',
            'url' => 'required|url|max:191|unique:feeds',
        ]);

        $feed = new Feed;
        $feed->title = $request->title;
        $feed->url = Feed::removeTrailedSlash($request->url);
        $saved = $feed->save();

        if (!$saved) {
            \Session::flash('error', 'Feed unsuccessfully added.');
            return back()->withInput();
        }

        \Session::flash('success', 'Feed successfully added.');
        return redirect()->route('feeds.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        return view('feed.edit', [
            'title' => 'Feed ID: ' . $feed->id,
            'feed' => $feed,
            'articles' => Feed::loadFeedsFromUrl($feed->url)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
        $request->validate([
            'title' => 'required|max:191',
            'url' => 'required|url|max:191' . ($feed->url == Feed::removeTrailedSlash($request->url) ? '' : '|unique:feeds'),
        ]);

        $feed->title = $request->title;
        $feed->url = Feed::removeTrailedSlash($request->url);
        $saved = $feed->save();

        if (!$saved) {
            \Session::flash('error', 'Feed unsuccessfully updated.');
            return back()->withInput();
        }

        \Session::flash('success', 'Feed successfully updated.');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
        $deleted = $feed->delete();

        if (is_null($deleted) || $deleted === true) {
            \Session::flash('success', 'Feed successfully deleted.');
            return redirect()->route('feeds.index');
        }

        \Session::flash('error', 'Feed unsuccessfully deleted.');
        return back();
    }
}
