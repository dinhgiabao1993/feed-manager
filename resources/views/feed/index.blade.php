@extends('layouts.master')

@section('header_buttons')
    @parent
    <div class="col-sm-12">
        <div class="margin-top-15px text-right">
            <a href="{{ route('feeds.create') }}" class="btn btn-outline-info">
                <i class="fa fa-plus"></i>
                New Feed
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-sm-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Url</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @if (count($feeds))
                @foreach ($feeds as $index => $feed)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td><a href="{{ route('feeds.edit', ['feed' => $feed->id]) }}">{{ $feed->title }}</a></td>
                        <td><a target="blank" href="{{ $feed->url }}">{{ $feed->url }}</a></td>
                        <td>
                            {!! Form::model($feed, ['route' => ['feeds.update', $feed->id], 'method' => 'delete']) !!}
                            <button class="btn btn-outline-danger" onclick="return confirm('Are you sure about deleting this feed?')" type="submit">
                                <i class="fa fa-trash"></i>
                                Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <th scope="row" colspan="4" class="text-center">No feeds found</th>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@endsection