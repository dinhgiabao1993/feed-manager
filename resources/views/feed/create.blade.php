@extends('layouts.master')

@section('content')
    <div class="col-sm-12">
        {!! Form::model($feed, ['route' => 'feeds.store']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', old('title', $feed->title), ['class' => 'form-control ' . ($errors->has('title') ? 'is-invalid' : '')]) !!}
            @error('title')
            <div class="text-danger">
                <small>{{ $message }}</small>
            </div>
            @enderror
        </div>

        <div class="form-group">
            {!! Form::label('url', 'Url') !!}
            {!! Form::text('url', old('url', $feed->url), ['class' => 'form-control ' . ($errors->has('url') ? 'is-invalid' : '')]) !!}
            @error('url')
            <div class="text-danger">
                <small>{{ $message }}</small>
            </div>
            @enderror
        </div>

        <div class="text-center">
            <button class="btn btn-success" type="submit">
                <i class="fa fa-plus"></i>
                Create Feed
            </button>
            <a class="btn btn-warning" href="{{ route('feeds.index') }}">
                <i class="fa fa-chevron-left"></i>
                Back
            </a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection