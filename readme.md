# Feed Manager

Simple RSS Feed manager

## Setup

- Clone repo or download it directly:
    ```
    git clone https://dinhgiabao1993@bitbucket.org/dinhgiabao1993/feed-manager.git
    ```

- Access repo directory and install dependencies:
    ```
    composer install
    ```

- Create new .env file, edit database access info. [Example here](https://github.com/laravel/laravel/blob/master/.env.example)

- Generate app key:
    ```
    php artisan key:generate
    ```

- Create Database

- Create tables:
    ```
    php artisan migrate
    ```

## Usage

- GUI
    - Start app:
    ```
    php artisan serve
    ```
    - Run app on listening host/port, default is:
    ```
    http://localhost:8000
    ```

- CLI
    - List Feeds command:
    ```
    php artisan feed:list
    ```
    - Read Feed by ID command:
    ```
    php artisan feed:read {id}
    ```
    - Read Feed by URL command:
    ```
    php artisan feed:read-url {url}
    ```
    - Add Feed command:
    ```
    php artisan feed:add
    ```
    - Delete Feeds by IDs command:
    ```
    php artisan feed:delete {ids*}
    ```